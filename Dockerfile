FROM node:18-alpine as builder

WORKDIR /app

COPY src src
COPY package.json package.json
COPY .npmrc .npmrc

RUN npm install
RUN npm run build

FROM nginx

COPY --from=builder /app/src/.vuepress/dist /usr/share/nginx/html
