# EUPT Website

...

## Getting started

### Prerequisites

* node 18

```sh
git clone https://gitlab.gwdg.de/subugoe/eupt/website.git
cd website
docker build -t eupt-website .
docker run --rm -p 8080:80 eupt-website
```
