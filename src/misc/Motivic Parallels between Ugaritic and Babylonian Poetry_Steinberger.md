---
title: Motivic Parallels between Ugaritic and Babylonian Poetry_Steinberger
layout: Layout
hide_sidebar: true
---

# {{ $frontmatter.title }}

Some motifs from Ugaritic poetry resemble motifs known from Babylonian poetic texts. Here is a list of examples:

(Work in progress; last updated on 08.11.2023)

## KTU 1.17-18 (ˀAqhatu) and Gilgameš VI:
In KTU 1.17 VI # ff., the goddess Anat offers the lad Aqhat eternal life in exchange for his bow. The scene is reminiscent of the passage from the sixth tablet of the Babylonian Gilgameš epic, where the goddess Ištar proposes to Gilgameš, promising him godlike life. Both Aqhat and Gilgameš reject the goddess’s offer, incurring her wrath. The goddess then heads to one of the chief gods of the pantheon, El in the Aqhat epic and Anu in the Gilgameš epic, and force permission to kill the lad.

The scribe responsible for elaborating the Aqhat epic in the form we have, probably knew the episode from the Gilgameš epic and shaped the Ugaritic episode – consciously or unconsciously – along the framework provided by the Gilgameš episode. The framework of the Babylonian episode was placed in a new context, linked to local religious ideas and literary images, and transposed to other protagonists. The basic motif is then linked with formulaic phrases that recur in Ugaritic poetry and augmented with other motifs.

## KTU 1.14 VI 4-25 (&) and Enlil and Namzitarra (Ug.1), 3'–11':
In two passages in the Kirtu epic, Kirtu is offered riches, namely gold, silver, horses, a chariot, and a servant. Kirtu asks what material riches would do for him. He does not want gold or silver, but a wife who will grant him an heir to the throne. The episode is reminiscent of a passage from the Sumerian-Akkadian text Enlil and Namzitarra, also copied in Ugarit: Here, Enlil offers Namzitarra silver, lapis lazuli, cattle, and sheep. But Namzitarra asks what all this is good for. After all, he would have to die sooner or later anyway.

In both the Kirtu epic and Enlil and Namzitarra, a man is offered material wealth, including silver and livestock. In both cases, the offer is rejected. In Enlil and Namzitarra, the scene illustrates the so-called vanity theme, that is, the idea of the futility of material possessions in the face of the transience of life. The same idea is behind the episode from the Kirtu epic, although it is not explicitly stated here: Kirtu does not desire gold or silver, but a wife to beget a descendant. The king needs a son to continue his dynasty and to hold the regular mortuary rituals after his death. Thus, what Kirtu desires extends beyond his death. In contrast, material wealth is transient and worthless.

It is striking that the two episodes are not only similar in content but are implemented similarly in terms of language and style: The Akkadian verb leqû “to take,” which occurs in Enlil and Namzitarra as alqe, is etymologically equivalent to the Ugaritic verb laqāḥu “to take,” which is attested in the Kirtu episode in the imperative form qaḥ. Both texts refer to kaspa, the common Semitic word for “silver;” in both texts, the protagonist’s response repeats the list of goods offered; and in both texts, the protagonist rejects the offer by means of a rhetorical question.

In the Kirtu epic, the Babylonian motif was transposed into a new context. It is uncertain, whether the scribe responsible for the elaboration of the Ugaritic Kirtu epic in the present form unconsciously incorporated the Babylonian image into Ugaritic poetry or whether he consciously alluded to the episode from Enlil and Namzitarra in this case. Given the close linguistic and stylistic parallels, I am inclined to assume a deliberate literary allusion: Although the Kirtu episode may be understood without knowledge of the Sumerian-Akkadian text, the imaginative world behind Kirtu’s dismissive attitude unfolds only against the background of the Babylonian wisdom tradition reflected in Enlil and Namzitarra. Only then does it become clear why material possessions seem completely worthless to Kirtu.
