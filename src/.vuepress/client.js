import { defineClientConfig } from '@vuepress/client';
import TidoPage from "./components/TidoPage.vue";
import Layout from "./layouts/Layout.vue";

export default defineClientConfig({
  async enhance({ app }) {
    if (!__VUEPRESS_SSR__) {
      await import('@subugoe/tido/dist/tido');
      await import('@subugoe/tido/dist/tido.css');
      app.component('TidoPage', TidoPage);
    }
  },
  layouts: {
    Layout,
  },
})
