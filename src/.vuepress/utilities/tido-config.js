const tidoConfig = {
    collection: "https://ahiqar.uni-goettingen.de/api/textapi/ahikar/arabic-karshuni/collection.json",
    container: "#viewer",
    colors: {
      primary: "#234ea6"
    },
    labels: {
        item: "Sheet",
        manifest: "Manuscript"
    },
    panels: [
        {
            label: "contents_and_metadata",
            views: [
                {
                    id: "tree",
                    label: "contents",
                    connector: {
                        id: 1
                    }
                },
                {
                    id: "metadata",
                    label: "metadata",
                    connector: {
                        id: 2,
                        options: {
                            collection: {
                                all: true
                            },
                            manifest: {
                                all: true
                            },
                            item: {
                                all: true
                            }
                        }
                    }
                }
            ]
        },
        {
            label: "image",
            views: [
                {
                    id: "image",
                    label: "Image",
                    connector: {
                        id: 3
                    }
                }]
        },
        {
            label: "text",
            views: [
                {
                    id: "text1",
                    label: "Transcription",
                    default: true,
                    connector: {
                        id: 4,
                        options: {
                            type: "transcription"
                        }
                    }
                },
                {
                    id: "text2",
                    label: "Transliteration",
                    connector: {
                        id: 4,
                        options: {
                            type: "transliteration"
                        }
                    }
                }
            ]
        },
        {
            label: "annotations",
            views: [
                {
                    id: "annotations1",
                    label: "Editorial",
                    connector: {
                        id: 5,
                        options: {
                            types: [
                                {
                                    name: "Person",
                                    icon: "biPersonFill"
                                },
                                {
                                    name: "Place",
                                    icon: "biGeoAltFill"
                                },
                                {
                                    name: "Editorial Comment",
                                    icon: "biChatFill"
                                },
                                {
                                    name: "Reference",
                                    icon: "biBoxArrowUpRight"
                                }
                            ]
                        }
                    }
                },
                {
                    id: "annotations2",
                    label: "Motif",
                    connector: {
                        id: 5,
                        options: {
                            types: [
                                {
                                    name: "Motif",
                                    icon: "biPenFill"
                                }
                            ]
                        }
                    }
                }
            ]
        }
    ],
    translations: {
        en: {
            contents_and_metadata: "Contents & Metadata"
        }
    }
};

export {
    tidoConfig
};
