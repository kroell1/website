import { defaultTheme } from '@vuepress/theme-default'
import { defineUserConfig } from 'vuepress'

export default defineUserConfig({
    title: 'Edition des ugaritischen poetischen Textkorpus',
    theme: defaultTheme({
        lastUpdated: false,
        contributors: false,
        sidebar: false,
        navbar: [
          { text: 'Start', link: '/' },
          {
          text: 'Miszellen',
          children : [
            {text: 'Motivic Parallels between Ugaritic and Babylonian Poetry', link: '/misc/Motivic Parallels between Ugaritic and Babylonian Poetry_Steinberger.html'}
          ]
        },
        { text: 'Edition', link: '/arabic-karshuni.html' }
      ]
    })
})
