Gegenstand des Langfristvorhabens ist eine digitale Open-access-Edition aller bislang bekannten poetischen Texte in ugaritischer Sprache, in der die epigraphischen Grundlagen und philologisch-poetologischen Entscheidungen samt der möglichen Alternativen dokumentiert sind.

Edition: Zu Beginn werden die Tafeln autographiert und transliteriert. Die Tafeln, die im Musée du Louvre und im British Museum verwahrt sind, werden kollationiert; die Tafeln, die sich in Syrien befinden und z.Zt. nicht zugänglich sind, werden auf Grundlage der verfügbaren Fotos, Faksimiles und Abgüsse bearbeitet. Anschließend werden die Texte vokalisiert, übersetzt, kommentiert und morphologisch sowie syntaktisch analysiert. Unterschiedliche Rekonstruktionsmöglichkeiten werden erläutert. Gleichzeitig werden Fotos und Autographien gesammelt und katalogisiert, Daten zur Materialität und zum Fundort der Tafeln zusammengestellt. Außerdem entsteht eine umfassende Bibliographie.

Poetologische Analyse: Die Edition der Texte wird mit einer poetologischen Analyse verzahnt: Die Versstruktur wird analysiert, parallele Versmuster werden rekonstruiert und stilistische Merkmale beleuchtet; daneben wird das Motivnetzwerk, das die ugaritische Poesie inhaltlich prägt, untersucht. Um die Texte kultur- und literaturgeschichtlich zu kontextualisieren, werden wiederkehrende Charakteristika mit den Literaturen der Nachbarkulturen verglichen. Der Fokus liegt auf der akkadischen Überlieferung aus der Spätbronzezeit sowie auf alttestamentlichen Texten.

Webportal und Publikation: Sämtliche Ergebnisse werden online und open-access veröffentlicht. Die SUB Göttingen wird eine innovative Editionsplattform entwickeln, auf der die Texte unter verschiedensten Gesichtspunkten annotiert werden können. Edition und poetologische Analyse werden auf dem EUPT-Webportal abgebildet und erstmalig mit digitalen Werkzeugen verknüpft, die komplexe Suchabfragen ermöglichen.

Die Texteditionen werden fortlaufend auf dem Webportal publiziert und nach der Veröffentlichung stetig überarbeitet. Die deutschen Übersetzungen von ˀAqhatu, Kirtu und Baˁlu samt allgemein verständlicher Einleitung und Erläuterung werden zudem in der Ausgabe „Das Baal-Epos und andere Erzählungen aus dem alten Syrien“ bei C.H. Beck veröffentlicht (Erscheinungsjahr: 2027).

Göttingen und die internationale Ugaritistik: Der deutschsprachigen Ugaritistik fehlt seit dem Ende der Ugarit-Forschungsstelle Münster ein institutioneller Mittelpunkt. EUPT hat sich zum Ziel gesetzt, zur Fortführung der deutschen und europäischen Ugaritistik beizutragen. Gleichzeitig soll das Projekt die weltweite Zusammenarbeit stärken: EUPT wird von einem internationalen Advisory Board wissenschaftlich begleitet und arbeitet eng mit dem Chicagoer Ugarit-Projekt Ras Shamra Tablet Inventory (RSTI) zusammen.

Arbeitsplan: EUPT ist in drei Phasen gegliedert: In den ersten drei Jahren werden die Gedichte von ˀAqhatu (KTU 1.17-1.19), den „Rephaim“ (KTU 1.20-1.22) und Kirtu (KTU 1.14-1.16) bearbeitet. Die zweite Phase gilt dem Baˁlu-Zyklus (KTU 1.1-1.6). In der dritten Phase bearbeitet EUPT die kürzeren mythologischen Texte, Gebete, Beschwörungen und die Ritualtexte, die poetische Formen enthalten. Das Korpus umfasst 68 Tafeln mit fast 4.000 Zeilen.Text für die Startseite.

DFG-Projekt: Sachbeihilfe / Langfristvorhaben; Projektnummer: 506107876
Projektstart: 01.08.2023
Projektleiter: Prof. Dr. Reinhard Müller / Prof. Dr. Wolfram Horstmann
Koordinator: Clemens Steinberger
Mitarbeitende: Noah Kröll (wiss. Mitarbeiter)
Janna Bösenberg (stud. Hilfskraft)
Christiane Meinhold (stud. Hilfskraft)
Marion Possiel (stud. Hilfskraft)
Frilla Smilgies (stud. Hilfskraft)
